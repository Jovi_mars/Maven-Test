package com.maven.user;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.util.List;

/**
 * @Name: GupaoMojo
 * =======================
 * @Description: 自定义插件
 * =======================
 * @Author: Jovi
 * =======================
 * @Date: Created in 2020-09-28
 * =======================
 * @Version: 0.0.1
 */
@Mojo(name = "customedu", defaultPhase = LifecyclePhase.PACKAGE)
public class CustomMojo extends AbstractMojo {

    @Parameter
    private String msg;

    @Parameter
    private List<String> options;

    @Parameter(property = "args")
    private String args;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        System.out.println("*********************custom plugin**********************" + msg);
        System.out.println("*********************custom plugin**********************" + options);
        System.out.println("*********************custom plugin**********************" + args);
    }
}
