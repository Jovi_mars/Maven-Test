package com.maven.user;

import com.maven.user.model.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * HelloServlet--MavenTest123
 *
 * @author: Jovi
 * @createTime: 2018-10-10 14:54
 **/
public class HelloServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("user", new User("admin", "123", "管理员1234"));
        req.getRequestDispatcher("/hello.jsp").forward(req,resp);
    }
}
