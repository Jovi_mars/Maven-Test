package com.maven.user.service;


import com.maven.user.model.User;

/**
 * IUserService--MavenTest
 *
 * @author: Jovi
 * @createTime: 2018-09-27 10:58
 **/
public interface IUserService {

    void add(User user);

    User loadByUserName(String userName);
}
