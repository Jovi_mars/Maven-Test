package com.maven.user.service;

import com.maven.user.dao.IUserDao;
import com.maven.user.model.User;

/**
 * IUserServiceImpl--MavenTest
 *
 * @author: Jovi
 * @createTime: 2018-09-27 10:59
 **/
public class IUserServiceImpl implements IUserService {

    private IUserDao userDao;

    public IUserServiceImpl(IUserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void add(User user) {
        userDao.add(user);
    }

    @Override
    public User loadByUserName(String userName) {
        return userDao.loadByUserName(userName);
    }
}
