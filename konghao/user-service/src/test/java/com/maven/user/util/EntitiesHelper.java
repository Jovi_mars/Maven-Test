package com.maven.user.util;

import com.maven.user.model.User;
import org.junit.Assert;

public class EntitiesHelper {
    private static User baseUser = new User(1, "admin", "123", "管理员");

    public static void assertUser(User expected, User actual) {
        Assert.assertNotNull(expected);
		Assert.assertEquals(expected.getId(), actual.getId());
        Assert.assertEquals(expected.getUserName(), actual.getUserName());
        Assert.assertEquals(expected.getPassword(), actual.getPassword());
        Assert.assertEquals(expected.getNickName(), actual.getNickName());
    }

    public static void assertUser(User expected) {
        assertUser(expected, baseUser);
    }
}
