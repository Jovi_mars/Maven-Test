package com.maven.user.service;

import com.maven.user.dao.IUserDao;
import com.maven.user.model.User;
import com.maven.user.util.EntitiesHelper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.easymock.EasyMock.*;

/**
 * userServiceTest--MavenTest
 *
 * @author: Jovi
 * @createTime: 2018-09-27 11:02
 **/
public class userServiceTest {

    private IUserDao userDao;

    private IUserService userService;

    private User baseUser;

    @Before
    public void setUp() {
        userDao = createMock(IUserDao.class);
        userService = new IUserServiceImpl(userDao);
        baseUser = new User("admin", "123", "管理");
    }

    @Test
    public void testAdd() {

        userDao.add(baseUser);
        expectLastCall();
        replay(userDao);
        userService.add(baseUser);

    }

    @Test
    public void testLoadByUserName() {
        expect(userDao.loadByUserName("admin")).andReturn(baseUser);
        replay(userDao);
        User tu = userService.loadByUserName("admin");
        EntitiesHelper.assertUser(tu, baseUser);
    }

    @After
    public void tearDown() {
        verify(userDao);
    }
}
