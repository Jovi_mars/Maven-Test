package com.maven.user.dao;

import com.maven.user.model.User;
import com.maven.user.util.AbstractDbUnitTestCase;
import com.maven.user.util.EntitiesHelper;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;

/**
 * userDaoTest--MavenTest
 *
 * @author: Jovi
 * @createTime: 2018-09-26 14:29
 * <p>
 * 如果执行mvn test 后数据库中文乱码，就需要在以下maven插件中加上编码
 *
 * <plugin>
 * <artifactId>maven-surefire-plugin</artifactId>
 * <version>2.20.1</version>
 * <configuration>
 * <argLine>-Dfile.encoding=UTF-8</argLine>
 * </configuration>
 * </plugin>
 **/
public class userDaoTest extends AbstractDbUnitTestCase {

    private IUserDao iUserDao;

    /**
     * 初始化设置
     * @throws IOException
     * @throws DataSetException
     */
    @Before
    public void setUp() throws IOException, DataSetException {
        iUserDao = new IUserDaoImpl();
        backupOneTable("t_user");
    }

    @Test
    public void testLoad() throws DatabaseUnitException, SQLException {
        IDataSet ds = createDateSet("t_user");
        DatabaseOperation.CLEAN_INSERT.execute(dbConnection, ds);

        User admin = iUserDao.loadByUserName("admin");
        EntitiesHelper.assertUser(admin);

    }

    @Test
    public void testAdd() throws DatabaseUnitException, SQLException {

        IDataSet ds = createDateSet("t_user");
        DatabaseOperation.TRUNCATE_TABLE.execute(dbConnection, ds);

        User user = new User("admin", "123", "管理员");
        iUserDao.add(user);
        Assert.assertTrue(user.getId() > 0);

        User tu = iUserDao.loadByUserName("admin");
        EntitiesHelper.assertUser(tu, user);
    }

    /**
     * 拆卸
     *
     * @throws DatabaseUnitException
     * @throws SQLException
     * @throws IOException
     */
    @After
    public void tearDown() throws DatabaseUnitException, SQLException, IOException {
        resumeTable();
    }
}
