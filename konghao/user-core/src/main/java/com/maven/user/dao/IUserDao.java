package com.maven.user.dao;

import com.maven.user.model.User;

/**
 * IUserDao--MavenTest
 *
 * @author: Jovi
 * @createTime: 2018-09-26 14:08
 **/
public interface IUserDao {

    void add(User user);

    User loadByUserName(String userName);
}
