package com.maven.user.dao;

import com.maven.user.util.HibernateUtil;
import com.maven.user.model.User;
import org.hibernate.Session;

/**
 * IUserDaoImpl--MavenTest
 *
 * @author: Jovi
 * @createTime: 2018-09-26 14:20
 **/
public class IUserDaoImpl implements IUserDao {

    @Override
    public void add(User user) {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.save(user);
        session.getTransaction().commit();
    }

    @Override
    public User loadByUserName(String userName) {

        Session session = HibernateUtil.getSession();
        // 将HQL语句中的"?"改为JPA-style:?1

        return (User) session.createQuery("from User where userName = ?1")
                .setParameter(1, userName).uniqueResult();
    }
}
